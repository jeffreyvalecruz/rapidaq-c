﻿/* PID class architecture for a scalable PID system using NI-DAQ tasks for input/output.
 *  
 * RapidAQ interfaces to the PR Test Engine API, allowing direct update of GUI controls. 
 *  
 * Author: Jeffrey Vale Cruz
 * Date  : 29/7-2016
 * 
 * 
 * */

using System.Linq;

namespace RapidAQ
{
    class PID_t
    {
        //PID parameters
        private string m_ID;
        private float m_SetPoint;
        private float m_KcGain;
        private float m_TiGain;
        private float m_TdGain;
        private float[] m_ProcessError = new float[4];
        private float m_IntegrationErrorFeedback;
        private float m_OutputFeedback;

        //Input paramaters
        private float m_InputMin;
        private float m_InputMax;
        private float m_InputPostScalingMin;
        private float m_InputPostScalingMax;
        private bool m_UseInputScaling;

        //Output parameters
        private float m_OutputMin;
        private float m_OutputMax;

        //Scaling parameters
        private float m_InputSlopeScaling;
        private float m_InputConstantScaling;
        private float m_OutputSlopeScaling;
        private float m_OutputConstantScaling;

        //Dynamic parameters
        private float m_SetPointJitter;
        private long m_TickCount;
        private bool m_FirstCall;
        private bool m_QuitFlag;

        //PR Test Engine parameters
        private bool m_UpdatePRTestEngine;
        private NationalInstruments.TestStand.Interop.API.Thread m_TestStandIThread;
        private string m_InRangeBooleanViewName;
        private bool m_InRangeBooleanViewSignaling;
        private string m_CurrentProcessDBLViewName;
        private bool m_CurrentProcessDBLViewSignaling;

        //DAQ parameters
        private NationalInstruments.DAQmx.AnalogSingleChannelWriter m_OutputWriter;
        private NationalInstruments.DAQmx.Task m_PIDInputTask;
        private NationalInstruments.DAQmx.Task m_PIDOutputTask;

        

        //Calculates elapsed time in seconds between calls
        private float ElapsedSeconds()
        {
            if (m_FirstCall)
            {
                m_TickCount = System.Environment.TickCount;
                return 0;
            }
            else
            {
                long elapsedTicks = System.Environment.TickCount - m_TickCount;
                m_TickCount = System.Environment.TickCount;
                float elapsedSeconds = elapsedTicks / 1000;
                //Log.Insert(Catagory.DEBUG, "{" + m_ID + "} Elapsed seconds since last call : " + elapsedSeconds.ToString());
                return elapsedSeconds;
            }
        }

        private void UpdatePRTestEngine(float processValue)
        {
            if (m_UpdatePRTestEngine)
            {
                int inRange = m_SetPoint - m_SetPointJitter <= processValue && m_SetPoint + m_SetPointJitter >= processValue ? 1 : 0;

                //Generating PR Test Engine event codes for invoking views.
                NationalInstruments.TestStand.Interop.API.UIMessageCodes InRangeViewEventNr = m_InRangeBooleanViewSignaling ?
                    NationalInstruments.TestStand.Interop.API.UIMessageCodes.UIMsg_UserMessageBase + 13 : NationalInstruments.TestStand.Interop.API.UIMessageCodes.UIMsg_UserMessageBase + 12;

                NationalInstruments.TestStand.Interop.API.UIMessageCodes CurrentTemperatureViewEventNr = m_CurrentProcessDBLViewSignaling ?
                    NationalInstruments.TestStand.Interop.API.UIMessageCodes.UIMsg_UserMessageBase + 9 : NationalInstruments.TestStand.Interop.API.UIMessageCodes.UIMsg_UserMessageBase + 8;

                m_TestStandIThread.PostUIMessageEx(InRangeViewEventNr, inRange, m_InRangeBooleanViewName, null, true);
                m_TestStandIThread.PostUIMessageEx(CurrentTemperatureViewEventNr, processValue, m_CurrentProcessDBLViewName, null, true);
            }
        }

        //Thread method of PID process for an PID instance with redundant input
        private async void Run(NationalInstruments.DAQmx.AnalogMultiChannelReader InputReader, int UpdateRate, float InputSpan)
        {
            m_PIDInputTask.Stop();
            m_PIDOutputTask.Stop();

            while (m_QuitFlag == false)
            {
                try
                {
                    m_PIDInputTask.Start();
                    m_PIDOutputTask.Start();

                    float currentSample;

                    double[] inputSamples = InputReader.ReadSingleSample();

                    if (m_UseInputScaling)
                    {
                        for (int i = 0; i < inputSamples.Length; i++)
                        {
                            inputSamples[i] = inputSamples[i] * m_InputSlopeScaling + m_InputConstantScaling;
                        }
                    }
                    

                    System.Array.Sort(inputSamples);

                    if (System.Math.Abs(inputSamples[inputSamples.Length-1] - inputSamples[0]) > InputSpan)
                    {
                        Log.Insert(Catagory.ERROR, $"PID {{{ m_ID }}} : Input span exceeded. - Test termination request sent to the PR Test Engine");
                        Log.Insert(Catagory.ERROR, $"PID {{{ m_ID }}} : Requested PR Test Engine to lower 'Ready to Run' flag");

                        if (m_UpdatePRTestEngine)
                        {
                            m_TestStandIThread.PostUIMessageEx(NationalInstruments.TestStand.Interop.API.UIMessageCodes.UIMsg_UserMessageBase + 2000, 0,
                            $"PID {{{ m_ID }}} : Input span exceeded. - Test termination request sent to the PR Test Engine", null, true);

                            m_TestStandIThread.PostUIMessageEx(NationalInstruments.TestStand.Interop.API.UIMessageCodes.UIMsg_UserMessageBase + 2010, 0,
                            $"PID {{{ m_ID }}} : Requested PR Test Engine to lower 'Ready to Run' flag", null, true);
                        }

                        currentSample = (float)inputSamples[inputSamples.Length - 1];
                    }
                    else
                    {
                        currentSample = (float)(inputSamples.Sum() / inputSamples.Length);
                    }

                    m_OutputWriter.WriteSingleSample(true, CalculateOutput(currentSample));

                    UpdatePRTestEngine(currentSample);

                    m_PIDInputTask.Stop();
                    m_PIDOutputTask.Stop();
                }
                catch (NationalInstruments.DAQmx.DaqException e)
                {
                    Log.Insert(Catagory.ERROR, $"{{{ m_ID }}} DAQ exception occured: { e.Message }.");
                }
                catch (System.Exception e)
                {
                    Log.Insert(Catagory.ERROR, $"{{{ m_ID }}} Exception occured: { e.Message }.");
                }

                await System.Threading.Tasks.Task.Delay(UpdateRate);
            }

            m_OutputWriter.WriteSingleSample(true, m_OutputMin);
        }


        //Thread method of PID process for an PID instance with single input
        private async void Run(NationalInstruments.DAQmx.AnalogSingleChannelReader InputReader, int UpdateRate)
        {
            while (m_QuitFlag == false)
            {
                try
                {
                    float currentSample = (float)InputReader.ReadSingleSample();

                    if (m_UseInputScaling)
                    {
                        currentSample = currentSample * m_InputSlopeScaling + m_InputConstantScaling;
                    }

                    m_OutputWriter.WriteSingleSample(true, CalculateOutput(currentSample));

                    UpdatePRTestEngine(currentSample);
                }
                catch (NationalInstruments.DAQmx.DaqException e)
                {
                    Log.Insert(Catagory.ERROR, $"{{{ m_ID }}} DAQ exception occured: { e.Message }.");
                }
                catch (System.Exception e)
                {
                    Log.Insert(Catagory.ERROR, $"{{{ m_ID }}} Exception occured: { e.Message }.");
                }

                await System.Threading.Tasks.Task.Delay(UpdateRate);
            }

            m_OutputWriter.WriteSingleSample(true, m_OutputMin);
        }

        
        //PID calculation
        private float CalculateOutput(float InputValue)
        {
            float T = ElapsedSeconds();

            if (m_FirstCall)
            {
                m_ProcessError[0] = m_SetPoint - InputValue;
                m_ProcessError[1] = m_ProcessError[0];
                m_ProcessError[2] = m_ProcessError[1];
                m_ProcessError[3] = m_ProcessError[2];
                m_FirstCall = false;
            }
            else
            {
                m_ProcessError[3] = m_ProcessError[2];
                m_ProcessError[2] = m_ProcessError[1];
                m_ProcessError[1] = m_ProcessError[0];
                m_ProcessError[0] = m_SetPoint - InputValue;
            }

            m_IntegrationErrorFeedback  = m_IntegrationErrorFeedback + (m_ProcessError[0] * T);

            //Calculate using higher order equation to reduce noise susceptibility 
            float deviation = ((m_ProcessError[0] + (3 * m_ProcessError[1]) - (3 * m_ProcessError[2]) - m_ProcessError[3]) / 6) / T;

            m_OutputFeedback = (m_KcGain * m_ProcessError[0]) + (m_TiGain * m_IntegrationErrorFeedback) + (m_TdGain * deviation);

            if (m_UseInputScaling)
            {
                m_OutputFeedback = ((m_OutputFeedback + m_InputPostScalingMin) * m_OutputSlopeScaling) + m_OutputConstantScaling;
            }

            if (m_OutputFeedback < m_OutputMin)
            {
                Log.Insert(Catagory.INFO, $"{{{ m_ID }}} Process: { InputValue.ToString() } -> PID -> { m_OutputFeedback.ToString() } |-> { m_OutputMin.ToString() }.");
                m_OutputFeedback = m_OutputMin;
            }
            else if (m_OutputFeedback > m_OutputMax)
            {
                Log.Insert(Catagory.INFO, $"{{{ m_ID }}} Process: { InputValue.ToString() } -> PID -> { m_OutputFeedback.ToString() } |-> { m_OutputMax.ToString() }.");
                m_OutputFeedback = m_OutputMax;
            }
            else
            {
                Log.Insert(Catagory.INFO, $"{{{  m_ID }}} Process: { InputValue.ToString() } -> PID -> { m_OutputFeedback.ToString() }.");
            }

            return m_OutputFeedback;
 
        }

        //PID constructor
        public PID_t(string ID,
                     float SetPoint,
                     float SetPointJitterSpan,
                     float InputMin,
                     float InputMax,
                     bool  UseInputScaling,
                     float InputPostScalingMin,
                     float InputPostScalingMax,
                     float OutputMin,
                     float OutputMax,
                     float KcGain,
                     float TiGain,
                     float TdGain,
                     string InputTaskName,
                     string OutputTaskName,
                     int UpdateRate,
                     bool RedundantInputTask                                            = false,
                     float InputSpan                                                    = 0,
                     NationalInstruments.TestStand.Interop.API.Thread TestStandIThread  = null,
                     string InRangeBooleanViewName                                      = null,
                     bool InRangeBooleanViewSignaling                                   = false,
                     string CurrentProcessDBLViewName                                   = null,
                     bool CurrentProcessViewSignaling                                   = false)
        {

            m_ID                                = ID;
            m_SetPoint                          = SetPoint;
            m_SetPointJitter                    = SetPointJitterSpan / 2;
            m_InputMin                          = InputMin;
            m_InputMax                          = InputMax;
            m_InputPostScalingMin               = InputPostScalingMin;
            m_InputPostScalingMax               = InputPostScalingMax;
            m_UseInputScaling                   = UseInputScaling;
            m_OutputMin                         = OutputMin;
            m_OutputMax                         = OutputMax;
            m_KcGain                            = KcGain;
            m_TiGain                            = TiGain;
            m_TdGain                            = TdGain;

            m_FirstCall                         = true;
            m_ProcessError[0]                   = 0;
            m_IntegrationErrorFeedback          = 0;

            m_TestStandIThread                  = TestStandIThread;
            m_InRangeBooleanViewName            = InRangeBooleanViewName;
            m_InRangeBooleanViewSignaling       = InRangeBooleanViewSignaling;
            m_CurrentProcessDBLViewName         = CurrentProcessDBLViewName;
            m_CurrentProcessDBLViewSignaling    = CurrentProcessViewSignaling;


            if (m_UseInputScaling)
            {
                try
                {
                    m_InputSlopeScaling     = (InputPostScalingMax - InputPostScalingMin) / (InputMax - InputMin);
                    m_InputConstantScaling  = InputPostScalingMin - (m_InputSlopeScaling * InputMin);

                    m_OutputSlopeScaling    = (OutputMax - OutputMin) / (InputPostScalingMax - InputPostScalingMin);
                    m_OutputConstantScaling = OutputMin - (m_OutputSlopeScaling * InputPostScalingMin);
                }
                catch (System.ArithmeticException e)
                {
                    Log.Insert(Catagory.ERROR, $"{{{ m_ID }}} Arithmetic exception thrown while calculating scaling parameters: { e.Message }.");
                }

                string inputConstant = m_InputConstantScaling < 0 ? m_InputConstantScaling.ToString() : "+" + m_InputConstantScaling.ToString();
                string outputConstant = m_OutputConstantScaling < 0 ? m_OutputConstantScaling.ToString() : "+" + m_OutputConstantScaling.ToString();

                Log.Insert(Catagory.INFO, $"{{{ m_ID }}} Using input scaling IN{{ y = { m_InputSlopeScaling }x { inputConstant } }}, OUT{{ y = { m_OutputSlopeScaling }x { outputConstant } }}.");
            }

            if (TestStandIThread != null)
            {
                m_UpdatePRTestEngine = true;
            }
            else
            {
                m_UpdatePRTestEngine = false;
            }

            Log.Insert(Catagory.DEBUG, $"{{{ m_ID }}} PID constructor called - starting thread. PID Parameters : Kc = { m_KcGain.ToString() }, Ti = { m_TiGain.ToString() }, Td = { m_TdGain.ToString() }, SP = { m_SetPoint.ToString()}.");
            
            try
            {
                m_PIDInputTask  = NationalInstruments.DAQmx.DaqSystem.Local.LoadTask(InputTaskName);
                m_PIDOutputTask = NationalInstruments.DAQmx.DaqSystem.Local.LoadTask(OutputTaskName);

                m_PIDInputTask.Control(NationalInstruments.DAQmx.TaskAction.Verify);
                m_PIDOutputTask.Control(NationalInstruments.DAQmx.TaskAction.Verify);

                m_OutputWriter = new NationalInstruments.DAQmx.AnalogSingleChannelWriter(m_PIDOutputTask.Stream);

                if (RedundantInputTask)
                {
                    NationalInstruments.DAQmx.AnalogMultiChannelReader InputReader = new NationalInstruments.DAQmx.AnalogMultiChannelReader(m_PIDInputTask.Stream);
                    
                    new System.Threading.Thread(() => Run(InputReader, UpdateRate, InputSpan)).Start();
                }
                else
                {
                    NationalInstruments.DAQmx.AnalogSingleChannelReader InputReader = new NationalInstruments.DAQmx.AnalogSingleChannelReader(m_PIDInputTask.Stream);

                    new System.Threading.Thread(() => Run(InputReader, UpdateRate)).Start();
                }
            }
            catch (NationalInstruments.DAQmx.DaqException e)
            {
                Log.Insert(Catagory.ERROR, $"{{{ m_ID }}} DAQ exception occured: { e.Message }.");
            }
            catch (System.Exception e)
            {
                Log.Insert(Catagory.ERROR, $"{{{ m_ID }}} Exception occured: { e.Message }.");
            }

        }

        //PID destructor
        ~PID_t()
        {
            m_OutputWriter.WriteSingleSample(true, m_OutputMin);
            m_PIDInputTask.Dispose();
            m_PIDOutputTask.Dispose();
            Log.Insert(Catagory.DEBUG, $"{{{ m_ID }}} PID destructor called - thread was closed.");
        }

        
        //Closes PID run thread
        public void RiseQuitFlag()
        {
            m_QuitFlag = true;
        }
    }
}
