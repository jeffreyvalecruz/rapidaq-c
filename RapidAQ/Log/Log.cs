﻿namespace RapidAQ
{
    enum Catagory { DEBUG, INFO, WARNING, ERROR }

    static class Log
    {
        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        extern static void OutputDebugStringA(string debugString);

        public static void Insert(Catagory catagory, string EntryText)
        {
            OutputDebugStringA("[Rapid.PID] [" + catagory.ToString() + "] " + EntryText);
        }
    }
}
