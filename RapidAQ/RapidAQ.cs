﻿/* RapidAQ Dynamic Link Library.
 * General purpose multi-PID handler with NI-DAQmx interfacing, to be called from any .NET supported platforms.
 * 
 * Requirements:
 * NI-DAQmx must be installed with support for .NET 4.5.1 (or below).
 * 
 * Author: Jeffrey Vale Cruz
 * Date  : 29/7-2016
 *  
 * */

namespace RapidAQ
{
    public sealed class RapidAQ
    {
        private System.Collections.Generic.Dictionary<string, PID_t> PIDThreads = new System.Collections.Generic.Dictionary<string, PID_t>();

        //Lazy initialization of Singleton pattern
        private static readonly System.Lazy<RapidAQ> lazy = new System.Lazy<RapidAQ>(() => new RapidAQ());
        
        public static RapidAQ ObtainRapidAQ { get { return lazy.Value; } }

        //public static RapidAQ _RapidAQ { get { return lazy.Value; } }

        private RapidAQ()
        {
            Log.Insert(Catagory.INFO, "RapidAQ constructor called");
        }

        ~RapidAQ()
        {
            Log.Insert(Catagory.INFO, "RapidAQ destructor called");
        }
        
        public string Version()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        //Quits PID instance
        //[System.STAThread]
        public void QuitPID(string ID)
        {
            PID_t IDInstance;
            if (PIDThreads.TryGetValue(ID, out IDInstance))
            {
                Log.Insert(Catagory.INFO, $"Quitting PID handler with ID: { ID }.");
                IDInstance.RiseQuitFlag();
                PIDThreads.Remove(ID);
                Log.Insert(Catagory.INFO, $"Remaining PID instances: { PIDThreads.Count.ToString() }.");
            }
            else
            {
                Log.Insert(Catagory.ERROR, $"Cannot quit PID handler - unknown ID: { ID }.");
            }
        }

        //Starts PID thread with single input
        //[System.STAThread]
        public void BeginSingleInputPID(string ID,
                                        float SetPoint,
                                        float SetPointJitterSpan,
                                        float InputMin,
                                        float InputMax,
                                        bool  UseInputScaling,
                                        float InputPostScalingMin,
                                        float InputPostScalingMax,
                                        float OutputMin,
                                        float OutputMax,
                                        float KcGain,
                                        float TiGain,
                                        float TdGain,
                                        string InputTaskName,
                                        string OutputTaskName,
                                        int    UpdateRate_ms,
                                        NationalInstruments.TestStand.Interop.API.Thread TestStandIThread,
                                        string InRangeBooleanViewName,
                                        bool   InRangeBooleanViewSignaling,
                                        string CurrentProcessDBLViewName,
                                        bool   CurrentProcessViewSignaling)
        {
            if (PIDThreads.ContainsKey(ID))
            {
                Log.Insert(Catagory.WARNING, "PID handler already exists - quitting existing PID handler...");
                QuitPID(ID);
            }
            
            Log.Insert(Catagory.INFO, $"PID handler starting: { ID }.");
            PIDThreads.Add(ID, new PID_t(ID, 
                                         SetPoint,
                                         SetPointJitterSpan,
                                         InputMin,
                                         InputMin,
                                         UseInputScaling,
                                         InputPostScalingMin,
                                         InputPostScalingMax,
                                         OutputMin, 
                                         OutputMax, 
                                         KcGain, 
                                         TiGain, 
                                         TdGain, 
                                         InputTaskName, 
                                         OutputTaskName, 
                                         UpdateRate_ms,
                                         false,
                                         0,
                                         TestStandIThread,
                                         InRangeBooleanViewName,
                                         InRangeBooleanViewSignaling,
                                         CurrentProcessDBLViewName,
                                         CurrentProcessViewSignaling                                                                                 
                                         ));
        }

        //Starts PID thread with redundant input
        //[System.STAThread]
        public void BeginRedundantInputPID(string ID,
                                           float SetPoint,
                                           float SetPointJitterSpan,
                                           float InputMin,
                                           float InputMax,
                                           bool UseInputScaling,
                                           float InputPostScalingMin,
                                           float InputPostScalingMax,
                                           float OutputMin,
                                           float OutputMax,
                                           float KcGain,
                                           float TiGain,
                                           float TdGain,
                                           string InputTaskName,
                                           string OutputTaskName,
                                           int UpdateRate_ms,
                                           float InputSpan,
                                           NationalInstruments.TestStand.Interop.API.Thread TestStandIThread,
                                           string InRangeBooleanViewName,
                                           bool InRangeBooleanViewSignaling,
                                           string CurrentProcessDBLViewName,
                                           bool CurrentProcessViewSignaling)
        {
            if (PIDThreads.ContainsKey(ID))
            {
                Log.Insert(Catagory.WARNING, "PID handler already exists - quitting existing PID handler...");
                QuitPID(ID);
            }

            Log.Insert(Catagory.INFO, $"PID handler starting: { ID }.");
            PIDThreads.Add(ID, new PID_t(ID,
                                         SetPoint,
                                         SetPointJitterSpan,
                                         InputMin,
                                         InputMax,
                                         UseInputScaling,
                                         InputPostScalingMin,
                                         InputPostScalingMax,
                                         OutputMin,
                                         OutputMax,
                                         KcGain,
                                         TiGain,
                                         TdGain,
                                         InputTaskName,
                                         OutputTaskName,
                                         UpdateRate_ms,
                                         true,
                                         InputSpan,
                                         TestStandIThread,
                                         InRangeBooleanViewName,
                                         InRangeBooleanViewSignaling,
                                         CurrentProcessDBLViewName,
                                         CurrentProcessViewSignaling
                                         ));
        }
    }
}
